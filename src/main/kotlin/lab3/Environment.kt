package lab3

abstract class Environment(vararg ags: Actor) {
    val agents: List<Actor> = ags.toList()

    fun step() {
        for (agent in agents) {
            sense(agent)
            processAction(agent, agent.act())
        }
    }

    abstract fun processAction(agent: Actor, act: Action)

    abstract fun sense(agent: Actor)
}

class FoodEnvironment(vararg args: Actor) : Environment(*args) {
    val scores: MutableMap<Actor, Int> = mutableMapOf()

    init {
        for (agent in args) {
            scores[agent] = 0
        }
    }

    override fun processAction(agent: Actor, act: Action) {
        when (act) {
            is ForageAction -> scores[agent] = scores[agent]!! + 1
        }
    }

    override fun sense(agent: Actor) {

    }
}