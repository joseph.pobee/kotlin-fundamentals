package lab3

abstract class Action {
}

class ForageAction : Action() {}

class HuntAction(name: String) : Action() {

}

class NoAction : Action()