package lab3

import java.util.Random

interface Actor {
    val name: String
    fun act(): Action
    fun perceive(vararg facts: Percept): Unit
}

class SimpleAgent(override val name: String) : Actor {

    override fun act(): Action {
        return ForageAction()
    }

    override fun perceive(vararg facts: Percept) {

    }

    override fun toString(): String {
        return name
    }
}

class RandomAgent(override val name: String, val prob: Double) : Actor {
    override fun act(): Action {
        if (Random().nextDouble() <= prob) {
            return ForageAction()
        }
        return NoAction()
    }

    override fun perceive(vararg facts: Percept) {
        TODO("Not yet implemented")
    }
}

fun main() {
    val env = FoodEnvironment(RandomAgent("Charlie", 0.8), RandomAgent("Bob", 0.2), RandomAgent("Alice", 0.6))
    for (i in 1..100) {
        env.step()
    }
    println(env.scores)
}