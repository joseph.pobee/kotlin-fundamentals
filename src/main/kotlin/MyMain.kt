import java.lang.Exception

fun main() {
    val human = Human()
    human.eat()
    human.printSpecies()
  }

fun add(a:Int, b:Int):Int {
    return a+b
}

fun addAge(age: Int) {
    var myName = try {1} catch (err:Exception) {2}
    if(age < 0) throw Exception()
}

enum class RoomType {
    Double,
    Twin,
    Suite
}

fun genNumber(type: RoomType): Int = when(type) {
    RoomType.Double -> 1
    RoomType.Suite -> 2
    RoomType.Twin -> 3
}

class Person(val name: String) {
    private val fullName: String

    init {
        println("This will run when I create it")
        fullName = name
    }
    constructor(userID:Int): this("First employee") {
        println("This should run after?")
    }

    companion object {
        val defaultGreeting: String = "Hi"
    }
}

open class Animal(spec:String) {
    val name:String

    init {
        name = "Lion"
    }

    open fun eat() {
        println("$name eating")
    }
}

class Bear(spec: String) : Animal(spec) {
    override fun eat() {
        println("Bear eating")
    }
}

interface Mammable {
    val numLegs: Int
    fun eat()
    fun printSpecies() {

    }
}

class Human(): Mammable {
    override val numLegs: Int = 2

    override fun eat() {
        println("Human eating")
    }
}