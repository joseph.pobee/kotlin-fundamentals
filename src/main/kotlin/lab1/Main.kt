package lab1

fun main() {
    intersection(-2,3,3,-2)
}

fun calculateY(m: Int, x:Int, c: Int ): Int {
    return m * x + c
}

fun intersection(mOne : Int, cOne : Int, mTwo : Int, cTwo : Int) : Unit {
    var intersectValue: Int? = null
    for (i in 0..100) {
        val p1: Int = calculateY(mOne, i, cOne);
        val p2: Int = calculateY(mTwo, i, cTwo);
        if(p1 == p2) {
            intersectValue = p1
            break
        }
    }
    if(intersectValue == null) {
        println("Don't intersect")
    } else {
        println(intersectValue)
    }
}