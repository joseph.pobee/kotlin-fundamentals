package lab2

var funds: Double = 100.0
val pswd = "password"

fun main() {
    var input: String
    var cmd: List<String>

    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        when (cmd[0]) {
            // Each command goes here...
            "balance" -> balance()
            "deposit" -> try {
                deposit(cmd[1].toDouble())
            } catch (err: Exception) {
                println("An error occurred while trying to deposit")
            }
            "withdraw" -> try {
                withdraw(cmd[1].toDouble())
            } catch (err: Exception) {
                println("An error occurred while trying to withdraw")
            }

            else -> println("Invalid command")
        }
    }
}

fun balance() {
    println(funds)
}

fun deposit(amt: Double) {
    funds += amt;
}

fun withdraw(amt: Double) {
    print("Please enter password: ")
    val password = readLine()
    if (password == pswd) {
        if (funds >= amt) {
            funds -= amt
            println("Debit successful")
        } else {
            println("Insufficient funds for debit")
        }
    } else println("Incorrect password")
}